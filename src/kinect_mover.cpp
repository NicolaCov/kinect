#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include <dynamic_reconfigure/server.h>
#include <kinect/dyn_paramsConfig.h>

#include <sstream>

ros::Publisher kinect_x,kinect_y,kinect_z,kinect_yaw,kinect_pitch,kinect_roll;

std_msgs::Float64 init_z,init_x,init_y,init_yaw,init_pitch,init_roll;

void callback(kinect::dyn_paramsConfig &config, uint32_t level) {
  std_msgs::Float64 z,x,y,yaw,pitch,roll;
  z.data = config.z;
  y.data = config.y;
  x.data = config.x;
  yaw.data = config.yaw;
  roll.data = config.roll;
  pitch.data = config.pitch;
 
  kinect_z.publish(z);
  kinect_y.publish(y);
  kinect_x.publish(x);
  kinect_yaw.publish(yaw);
  kinect_pitch.publish(pitch);
  kinect_roll.publish(roll);
}

void send_init_pose()
{
  kinect_z.publish(init_z);
  kinect_y.publish(init_y);
  kinect_x.publish(init_x);
  kinect_yaw.publish(init_yaw);
  kinect_pitch.publish(init_pitch);
  kinect_roll.publish(init_roll);
  ROS_INFO("init_pose sent");
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "talker");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  ros::Rate loop_rate(10);

  kinect_z = n.advertise<std_msgs::Float64>("kinect/prism_z_position_controller/command", 1);
  kinect_x = n.advertise<std_msgs::Float64>("kinect/prism_x_position_controller/command", 1);
  kinect_y = n.advertise<std_msgs::Float64>("kinect/prism_y_position_controller/command", 1);
  kinect_yaw = n.advertise<std_msgs::Float64>("kinect/yaw_position_controller/command", 1);
  kinect_pitch = n.advertise<std_msgs::Float64>("kinect/pitch_position_controller/command", 1);
  kinect_roll = n.advertise<std_msgs::Float64>("kinect_roll/roll_position_controller/command", 1);

  dynamic_reconfigure::Server<kinect::dyn_paramsConfig> server;
  dynamic_reconfigure::Server<kinect::dyn_paramsConfig>::CallbackType f;
  f = boost::bind(&callback, _1, _2);
  server.setCallback(f);

  n.param("init_z",init_z.data,0.0);
  n.param("init_x",init_x.data,0.0);
  n.param("init_y",init_yaw.data,0.0);
  n.param("init_yaw",init_yaw.data,0.0);
  n.param("init_pitch",init_pitch.data,0.0);
  n.param("init_roll",init_roll.data,0.0);
  send_init_pose();

  while (ros::ok())
  {    
    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}