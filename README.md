**Kinect Standalone Package For Gazebo Simulations**

This package simply provides a Kinect that can move along the environment for simulation with whatever world. 

# How to use it#
Compile the package and then launch the file kinect_standalone**_sim**.launch
```
#!

$ roslaunch kinect_standalone kinect_standalone_sim.launch 

```
All the relevant topics of the kinect are puslihed in the topics /camera/* exactly like a real kinect.

You can move the kinect through the dynamic reconfigure panel, or with rqt_gui.
To change the limit of workspace you have to change the parameters in dyn_parm.cfg and also the limits for the prismatic joints (the revolute ones are continuous). They name of the prismatics are: prism_x , prism_y, prism_z.

Remark: the urdf is built offline from the xacro model by running:
rosrun xacro xacro kinect.xacro > kinect.urdf

# To DO #
* An option for the kinect plugin misses, but the pointcloud works good, execpt for the rgb info. You can build the colored point cloud straightforward combinign the contents of the topics: /camera/rgb/image_raw and /camera/depth/points